# See [`greenlock-store-test`](https://git.rootprojects.org/root/greenlock-store-test.js)

That's the test.

## Reference implementations

* [`greenlock-store-fs`](https://git.rootprojects.org/root/greenlock-store-fs.js)
* [`greenlock-store-sequelize`](https://git.rootprojects.org/root/greenlock-store-sequelize.js)